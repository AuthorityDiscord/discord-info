# Moderation

Unlike some servers, we don't have "punishment guidelines". Staff are expected to make their own (fair) punishments. 

If we have an issue with your moderation an admin will contact you.

If Authority is offline you're welcome to manually ban. If you manually ban please ensure you inform an admin so they can post a message in the modlogs channel.