# Support

We use Dyno for tags. You can get a list of all the tags available by saying `?tags`.


You can use `?dtag [tag name]` to automatically delete the command after using the tag.

Say `?sup` to add or remove the `Support` role from yourself.