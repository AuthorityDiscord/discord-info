# Announcements:

Please be professional in #announcements, don't ping @everyone or @here for stupid reasons, and use proper grammar and spelling!

Please be proper with announcements (also, try and follow our template `**:emote: | Title:**`). 
If the bot is offline and you're waiting on Bird for a restart, ensure that you tell users you're waiting for the developer and __NOT to ping / DM__ Bird or any staff.